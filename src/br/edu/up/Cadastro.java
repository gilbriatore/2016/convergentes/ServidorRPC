package br.edu.up;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Cadastro extends HashMap<Integer, List<Produto>> {
	
	private static final long serialVersionUID = 932143907511556236L;

	public Cadastro() {
		List<Produto> alimentos = new ArrayList<>();
		alimentos.add(new Produto(1, "Arroz", "Arroz parboilizado"));
		alimentos.add(new Produto(2, "Feij�o", "Feij�o mineiro"));
		alimentos.add(new Produto(3, "Farofa", "Farofa temperada"));
		
		List<Produto> pecas = new ArrayList<>();
		pecas.add(new Produto(1, "Pneu", "Pneu para trilha"));
		pecas.add(new Produto(2, "Correia", "Correia dentada"));
		pecas.add(new Produto(3, "Parfuso", "Parafuso sextavado"));
		
		put(1, alimentos);
		put(2, pecas);
	}
}
