package br.edu.up;

import java.io.IOException;
import java.util.List;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

public class ServidorRPC {

	public static String novaLinha = System.getProperty("line.separator");
	
	// Procedimento invocado remotamente.
	public static String listar(int categoria) {
		StringBuilder sb = new StringBuilder();
		List<Produto> lista = new Cadastro().get(categoria);
		for (Produto produto : lista) {
			sb.append(produto);
			sb.append(novaLinha);
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		
		Connection conexao = null;
		Channel canal = null;

		try {

			// Cria a conex�o e o canal.
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost("localhost");
			conexao = factory.newConnection();
			canal = conexao.createChannel();
			canal.queueDeclare("rpc", false, false, false, null);
			canal.basicQos(1);

			// Cria o consumidor e vincula � fila.
			QueueingConsumer consumidor = new QueueingConsumer(canal);
			canal.basicConsume("rpc", false, consumidor);

			System.out.println("Aguardando chamadas remotas!");

			// Faz as entregas.
			while (true) {
				String produtos = null;
				QueueingConsumer.Delivery entrega = consumidor.nextDelivery();
				
				String mensagem = new String(entrega.getBody(), "UTF-8");
				Integer categoria = Integer.parseInt(mensagem);
				
				produtos = listar(categoria);
				
				BasicProperties props = entrega.getProperties();
				BasicProperties respostaProps = new BasicProperties().builder()
						.correlationId(props.getCorrelationId())
						.build();
				
				canal.basicPublish("", props.getReplyTo(), 
						respostaProps, produtos.getBytes("UTF-8"));
				canal.basicAck(entrega.getEnvelope().getDeliveryTag(), false); 
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conexao != null){
				try {
					conexao.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}